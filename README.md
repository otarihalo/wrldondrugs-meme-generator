# About
This is the theme used to Develop the wrldondrugs meme generator page. Clone this theme in to a wordpress instance inside of htdocs and start MAMP to begin developing.

For Meme

# Development Setup

Navigate to this folder inside your terminal `/Applications/MAMP/htdocs/homekit/wp-content/themes/wrldondrugs`

1. In the gulpfile(line 26) make sure the port for localhost matches the port used by MAMP.
2. Open in VSC(we will only use this) `code .`.
3. Install Node Modules `npm install`.
4. Run Gulp. `npm run gulp`.

This will open a new window that is a proxy of the inital localhost:8888 site. All changes to PHP and JS will reload the page. All changes to SCSS will stream and update on page. Work within the /app folder.

#FTP

All CSS and JS files are inside dist folder minimized.
Meme page found in /pages folder.

