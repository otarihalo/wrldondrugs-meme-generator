$.get(`https://www.instagram.com/explore/tags/wrldondrugs/?hl=en`, function(sourceHTML) {
  let r = new RegExp('<script type="text\/javascript">' +
                '([^{]+?({.*profile_pic_url.*})[^}]+?)' +
                '<\/script>');
  let source = sourceHTML;
  let jsonStr = source.match(r)[2];
  let data = JSON.parse(jsonStr);
  let instaWrapper = $('.js-insta-wrapper');

  let socialPhotos = '';
  let photos = data['entry_data']['TagPage'][0].graphql.hashtag.edge_hashtag_to_media.edges;


  for( let i = 0; i < 16; i ++) {
    let photo = photos[i].node;
    socialPhotos +=
    `<a href="https://www.instagram.com/p/${photo.shortcode}" target="_blank" rel="noopener noreferrer">
      <div class="insta-photo" style="background-image: url('${photo.display_url}')">
        <div class="insta-photo-hover">
          <h6 class="insta-photo-hover-view">View</h6>
        </div>
      </div>
    </a>`;
  }

  instaWrapper.html(socialPhotos);
});


(function ($) {
	let canvas 		= document.getElementById('canvas'),
		context 	= canvas.getContext('2d'),
		preview 	= document.getElementById('preview-canvas'),
		contextP 	= preview.getContext('2d'),
		$upload		= $("#inputFile"),
		$image 		= $('.js-image'),
		imageWt		= 400,
		imageHt		= 400,
		$main 		= $('.main');

	function init() {
		canvas.width = imageHt;
		canvas.height = imageHt;
		preview.width = imageHt;
		preview.height = imageHt;
	}
	init();

	// upload image
	function readURL(input) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();

			reader.onload = function (e) {
	      // insert to img holder
				$image.attr('src', e.target.result).addClass('hide');

				$('.loading').removeClass('loading-non');

				setTimeout(function() {
					$('.loading .bar').addClass('run');
				}, 400);
				setTimeout(function() {
					$('.loading').addClass('hide');
					$('.generate-actions').removeClass('gone');
					$('.js-copy-title').html('Click to <br>upload again');
				}, 1600);

				$('#preview-canvas').removeClass('hide');
			}
			reader.readAsDataURL(input.files[0]);
		}
	}
	$upload.change(function () {
		readURL(this);
	});

  var $lightbox = $('.js-meme-lightbox');
  var $overlay = $('.overlay');
  var $close = $('.close');

  //create another meme/clear
  $('.js-create').on('click', function(e) {
    e.preventDefault();
    var $name = $('.name');
    var $addiction = $('.addiction');

    $name.val('');
    $addiction.val('');

    $lightbox.removeClass('show');
    $overlay.removeClass('show');
    $image.removeClass('hide');
    canvas.classList.add('hide');
  })
	// save to post
	$('.js-generate').on('click', function(e) {
		e.preventDefault();
    $main.addClass('main-generated');
    var $name = $('.name').val();
    var $addiction = $('.addiction').val();

    if($name.length === 0 || $addiction.length === 0) {
      var $error = $('.error-message');
      $error.html('<p>Please fill out name and addiciton to continue.</p>').addClass('show');

      setTimeout(function() {
        $error.removeClass('show');
      }, 1500);
    } else {

      // Reset canvas
      var base_image 		= new Image();
      base_image.src 		= $image.attr('src');

      base_image.onload 	= function() {

        // Add image
        context.drawImage(base_image, 0, 0, imageWt, imageHt );

        context.font = "24px Champion";
        context.textAlign = "center";
        context.textBaseline = "middle";
        context.fillStyle = "#d797ba";  //<======= and here
        context.fillText($name.toUpperCase(), 290,167);
        context.fillText($addiction.toUpperCase(), 290, 270);

          // Generate the image data
          var Pic = document.getElementById("canvas").toDataURL("image/png");
          Pic = Pic.replace(/^data:image\/(png|jpg);base64,/, "");

          var data = {
          action: 'upload',
          data_image : Pic
        };

        var $error = $('.error-message');
        $error.html('<p>Generating...</p>').addClass('show');

        // Sending the image data to Server to save
        $.post(ajaxURL, data, function(response) {
          var data = JSON.parse(response);

          if(data.status == 'success') {
            $('.js-download').attr('href', `${siteurl}/wp-content/uploads/meme/${data.title}.jpg`);

            setTimeout(function() {
              $error.removeClass('show');

              $lightbox.addClass('show');
              $overlay.addClass('show');
              $image.addClass('hide');
              canvas.classList.remove('hide');
            }, 500);

            $close.on('click', function() {
              $lightbox.removeClass('show');
              $overlay.removeClass('show');
              $image.removeClass('hide');
              canvas.classList.add('hide');
            })
          }
        });
      }
    }
	});

	$('.mob-upload').on('click', function(e){
		e.preventDefault();
		$('.tee-ball').trigger('click');
	});
}(jQuery));
