<?php

	// This theme styles the visual editor to resemble the theme style.
	// Add RSS feed links to <head> for posts and comments.
	add_theme_support( 'automatic-feed-links' );

	// Enable support for Post Thumbnails, and declare two sizes.
	add_theme_support( 'post-thumbnails' );

  // Enable session
	if (!session_id())
  session_start();

  // Add custom post types
  add_theme_support( 'post-thumbnails' );
  add_action( 'init', 'create_post_type' );
  function create_post_type() {
  register_post_type( 'meme',
    array(
      'labels' => array(
        'name' => __( 'Memes' ),
        'singular_name' => __( 'Meme' )
      ),
    'public' => true,
    'has_archive' => false,
    'supports' => array( 'title', 'thumbnail' )
    )
  );
  }

  function checkImage($imgData) {
    $f = finfo_open();
    $mime_type = finfo_buffer($f, $imgData, FILEINFO_MIME_TYPE);
    return $mime_type == 'image/png' || $mime_type == 'image/jpeg' || $mime_type == 'image/jpg';
  }

  // AJAX function to generate meme
  add_action( 'wp_ajax_upload', 'upload_function' );
  add_action( 'wp_ajax_nopriv_upload', 'upload_function' );

  function upload_function() {
    if(!empty($_POST)) {
      // Create session ID if it does not exist to use for image filename
      if(!isset($_SESSION['id'])) {
        $_SESSION['id'] = md5( time() );
      }

      $image_filename = $_SESSION['id'] . '.jpg';

      if (array_key_exists('data_image',$_REQUEST)) {
          $imgData = base64_decode($_POST['data_image']);

          //Check if it's an image
          if(checkImage($imgData)){

            // Path where the image is going to be saved
            $filePath = WP_CONTENT_DIR.'/uploads/meme/'.$image_filename;
            $attach_title 	= preg_replace( '/\.[^.]+$/', '', basename( $filePath ) );

            // Delete previously uploaded image
            if (file_exists($filePath)) { unlink($filePath); }

            // Write $imgData into the image file
            $file = fopen($filePath, 'w');
            fwrite($file, $imgData);
            fclose($file);

            // If the attachment exists in the Wordpress
          if( wp_get_attachment_by_post_name( $attach_title ) ) {
                $array = array(
                'status' => 'fail',
                'error' => 'Attachment exists in Wordpress'
              );

          // If the attachment does not exist in the Wordpress
            } else {
            // Create a new post
            $new_post = array(
              'post_title'    => $_SESSION['id'],
              'post_content'  => '',
              'post_status'   => 'draft',
              'post_type'     => 'meme'
              //'post_author'   => 1 // NEED TO ADD USER ID
            );

            // Insert the post into the database
            $post_id = wp_insert_post( $new_post, $wp_error );
            //update_field('field_548b3013f0e7e', $email, $post_id);
            //update_field('field_548b300ef0e7d', $fullname, $post_id);

            // Get the path to the upload directory.
            $wp_upload_dir = wp_upload_dir();

            $attachment = array(
              'guid'           => $wp_upload_dir['url'] . '/meme/' . basename( $filePath ),
              'post_mime_type' => 'image/jpeg',
              'post_title'     => preg_replace( '/\.[^.]+$/', '', basename( $filePath ) ),
              'post_content'   => '',
              'post_status'    => 'inherit'
            );

            // Insert the attachment.
            $attach_id = wp_insert_attachment( $attachment, $filePath, $post_id );

            // Make sure that this file is included, as wp_generate_attachment_metadata() depends on it.
            require_once( ABSPATH . 'wp-admin/includes/image.php' );

            // Set as featured image
            set_post_thumbnail($post_id, $attach_id);

            // Creating a response for the AJAX
            $array = array(
                'status' => 'success',
                'title' => get_the_title($post_id),
                'permalink' => get_permalink($post_id),
                'attachment' => $wp_upload_dir['baseurl'] . '/meme/' . $image_filename
              );

              session_unset();
              session_destroy();
            }
        }else{
          $array = array(
              'status' => 'fail',
              'error' => 'Post is not an image'
            );
        }
      }
    } else {
      $array = array(
          'status' => 'fail',
          'error' => 'Post is empty'
        );
    }

    $response = json_encode($array);
    die( $response );
  }

  function submit_function() {
    $image_filename = $_SESSION['id'] . '.jpg';
    $file_path 		= WP_CONTENT_DIR.'/uploads/meme/'.$image_filename;
    $attach_title 	= preg_replace( '/\.[^.]+$/', '', basename( $file_path ) );

    // If the attachment exists in the Wordpress
    if( wp_get_attachment_by_post_name( $attach_title ) ) {
          $array = array(
          'status' => 'fail',
        );

    // If the attachment does not exist in the Wordpress
    } else {
    // Create a new post
    $new_post = array(
      'post_title'    => $image_filename,
      'post_content'  => '',
      'post_status'   => 'draft',
      'post_type'     => 'meme'
      //'post_author'   => 1 // NEED TO ADD USER ID
    );

    // Insert the post into the database
    $post_id = wp_insert_post( $new_post, $wp_error );
    //update_field('field_548b3013f0e7e', $email, $post_id);
    //update_field('field_548b300ef0e7d', $fullname, $post_id);

    // Get the path to the upload directory.
    $wp_upload_dir = wp_upload_dir();

    $attachment = array(
      'guid'           => $wp_upload_dir['url'] . '/meme/' . basename( $file_path ),
      'post_mime_type' => 'image/jpeg',
      'post_title'     => preg_replace( '/\.[^.]+$/', '', basename( $file_path ) ),
      'post_content'   => '',
      'post_status'    => 'inherit'
    );

    // Insert the attachment.
    $attach_id = wp_insert_attachment( $attachment, $file_path, $post_id );

    // Make sure that this file is included, as wp_generate_attachment_metadata() depends on it.
    require_once( ABSPATH . 'wp-admin/includes/image.php' );

    // Set as featured image
    set_post_thumbnail($post_id, $attach_id);

    // Creating a response for the AJAX
    $array = array(
      'status' => 'success',
      'title' => get_the_title($post_id),
        'permalink' => get_permalink($post_id),
        'attachment' => $wp_upload_dir['baseurl'] . '/meme/' . $image_filename
      );

      session_unset();
      session_destroy();
    }

    $response = json_encode($array);
  return $response;
  }

  // Function to query attachment by post name
  if( ! ( function_exists( 'wp_get_attachment_by_post_name' ) ) ) {
    function wp_get_attachment_by_post_name( $post_name ) {
        $args = array(
            'post_per_page' => 1,
            'post_type'     => 'attachment',
            'name'          => trim ( $post_name ),
        );
        $get_posts = new Wp_Query( $args );

        if ( $get_posts->posts[0] )
            return true;
        else
            return false;
    }
  }
