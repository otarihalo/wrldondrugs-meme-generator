<?php
  /* Template Name: Wrldondrugs Meme Page */
  //http://intridea.github.io/sketch.js/
  //https://imgflip.com/memegenerator
  //http://www.codicode.com/art/upload_and_save_a_canvas_image_to_the_server.aspx
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="shortcut icon" type="image/png" href="<?php echo esc_url( get_stylesheet_directory_uri()); ?>/dist/images/favicon.png"/>
  <link rel="shortcut icon" type="image/png" href="<?php echo esc_url( get_stylesheet_directory_uri()); ?>/dist/images/favicon.png"/>
  <title>WRLDONDRUGS</title>
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,700" rel="stylesheet">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="<?php echo esc_url( get_stylesheet_directory_uri()); ?>/dist/main.min.css">

  <script type="text/javascript">
    var siteurl = '<?php echo site_url(); ?>',
        userID = '<?php echo get_current_user_id(); ?>',
        ajaxURL = '<?php echo admin_url('admin-ajax.php', is_ssl()?'https':'http'); ?>',
        dir = '<?php echo get_stylesheet_directory_uri(); ?>';
  </script>
</head>
<body <?php body_class() ?>>

<div class="overlay"></div>
<div class="error-message"></div>

<main class="page">
  <section class="banner">
    <img src="<?php echo esc_url( get_stylesheet_directory_uri()); ?>/dist/images/banner.png" alt="">
  </section>

  <section class="meme-form">
    <form runat="server">
      <div class="input-wrapper">
        <h2>I'm <input type="text" class="name js-require" name="name" placeholder="First Name?" require> and I'm Addicted to <input type="text" class="addiction js-require" name="addiction" placeholder="What is Your Addiction?" require>.</h2>
      </div>
      <div class="button-wrapper">
        <button class="submit js-generate" type="submit">Generate</button>
      </div>
    </form>
  </section>

  <section class="stream">
    <div class="album-player">

      <iframe src="https://open.spotify.com/embed/album/6P9PZjWXoCRF5b66BafPKY" width="300" height="80" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>

    </div>

    <a href="http://smarturl.it/WRLDONDRUGS" class="stream-btn" target="_blank"><i class="fa fa-music" aria-hidden="true"></i> Stream / Download</a>
  </section>

  <section class="download-lightbox js-meme-lightbox">
    <span class="close">x</span>
    <div class="wrapper">
      <h1 class="title">#WRLDONDRUGS</h1>
      <div class="col-left">
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/meme-bg2.jpg" alt="" class="js-image canvas-image" width="100%" height="385">
        <canvas id="preview-canvas" class="canvas hide" width="100%" height="400">Your browser does not support HTML5 Canvas objects</canvas>
        <canvas id="canvas" class="canvas hide" width="100%" height="400">Your browser does not support HTML5 Canvas objects</canvas>

        <a href="#" class="download js-download" download> <i class="fa fa-download" aria-hidden="true"></i> Download</a>
      </div>
      <div class="or-div">OR</div>
      <div class="col-right">
        <h2>Create another meme</h2>
        <a href="" class="create js-create">Create</a>
      </div>
    </div>
  </section>

  <section class="meme-posts">
     <?php $memes = new WP_Query( array('post_type' => 'meme', 'order' => 'DESC', 'orderby' => 'date', 'posts_per_page' => 16, 'post_status' => 'publish' ) ); ?>

     <?php if( $memes -> have_posts()) {?>
    <div class="container">
      <p class="meme-posts-scroll">Scroll <i class="fa fa-long-arrow-right" aria-hidden="true"></i> </p>

      <?php while( $memes -> have_posts()) { $memes -> the_post();?>
        <div class="meme-posts-post" style="background-image: url(<?php echo wp_get_attachment_url( get_post_thumbnail_id( $post->ID ) ); ?>);"></div>
      <?php } ?>
    </div>
     <?php } ?>
  </section>

  <section class="instagram">
    <div class="container">
      <h2 class="hashtag"><span class="fa fa-instagram"></span>#WRLDONDRUGS</h2>
      <div class="insta-wrapper js-insta-wrapper">

      </div>
    </div>
  </section>

  <section class="album">
    <div class="container">

      <div class="col-left">
        <div class="album-img">
          <img src="<?php echo esc_url( get_stylesheet_directory_uri()); ?>/dist/images/album.png" alt="">
        </div>
      </div>

      <div class="col-right">
        <div class="album-copy">
          <h3>Future & Juice World Present...</h3>
          <h1>Wrld On Drugs</h1>
        </div>

        <div class="providers">
          <ul class="providers-list">
            <li>
              <a href="http://smarturl.it/WRLDONDRUGS/itunes" class="provider" target="_blank">
                <img src="<?php echo esc_url( get_stylesheet_directory_uri()); ?>/dist/images/applestore.png" alt="">
              </a>
            </li>

            <li>
              <a href="http://smarturl.it/WRLDONDRUGS/applemusic" class="provider" target="_blank">
                <img src="<?php echo esc_url( get_stylesheet_directory_uri()); ?>/dist/images/applemusic.png" alt="">
              </a>
            </li>

            <li>
              <a href="http://smarturl.it/WRLDONDRUGS/spotify" class="provider" target="_blank">
                <img src="<?php echo esc_url( get_stylesheet_directory_uri()); ?>/dist/images/spotify.png" alt="">
              </a>
            </li>

            <li>
              <a href="http://smarturl.it/WRLDONDRUGS/googleplay" class="provider" target="_blank">
                <img src="<?php echo esc_url( get_stylesheet_directory_uri()); ?>/dist/images/googleplay.png" alt="">
              </a>
            </li>

            <li>
              <a href="http://smarturl.it/WRLDONDRUGS/az" class="provider" target="_blank">
                <img src="<?php echo esc_url( get_stylesheet_directory_uri()); ?>/dist/images/amazonmusic.png" alt="">
              </a>
            </li>

            <li>
              <a href="http://smarturl.it/WRLDONDRUGS/soundcloud" class="provider" target="_blank">
                <img src="<?php echo esc_url( get_stylesheet_directory_uri()); ?>/dist/images/soundcloud.png" alt="">
              </a>
            </li>

            <li>
              <a href="http://smarturl.it/WRLDONDRUGS/deezer" class="provider" target="_blank">
                <img src="<?php echo esc_url( get_stylesheet_directory_uri()); ?>/dist/images/deezer.png" alt="">
              </a>
            </li>

            <li>
              <a href="http://smarturl.it/WRLDONDRUGS/tidal" class="provider" target="_blank">
                <img src="<?php echo esc_url( get_stylesheet_directory_uri()); ?>/dist/images/tidal.png" alt="">
              </a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </section>

  <section class="socials">
    <ul class="socials-list">
      <li>
        <a href="https://www.facebook.com/FutureOfficial" class="socials-link fa fa-facebook-official" target="_blank"></a>
      </li>
      <li>
        <a href="https://twitter.com/FREEBANDGANG" class="socials-link fa fa-twitter" target="_blank"></a>
      </li>
      <li>
        <a href="https://www.instagram.com/future/" class="socials-link fa fa-instagram" target="_blank"></a>
      </li>
      <li>
        <a href="https://soundcloud.com/futureisnow" class="socials-link fa fa-soundcloud" target="_blank"></a>
      </li>
      <li>
        <a href="https://www.youtube.com/user/FreeBandGang/videos" class="socials-link fa fa-youtube-play" target="_blank"></a>
      </li>
    </ul>
  </section>
</main>

<footer id="footer" class="footer">
  <div class="label-logos">
    <div class="label-logo-inner"><a href="http://www.epicrecords.com" target="_blank"><img src="<?php echo esc_url( get_stylesheet_directory_uri()); ?>/dist/images/logo-epic.png" width="71" height="50" border="0" alt="Epic"></a></div>
  </div>
  <div class="copyright">
    © 2018 Sony Music Entertainment. All rights reserved.  | <a href="http://www.sonymusic.com/about/feedback.php" target="_blank">Send us Feedback</a>
    <br>
    <a href="http://www.sonymusic.com/privacypolicy.html" target="_blank">Privacy Policy</a> | <a href="http://www.sonymusic.com/privacy/termsandconditions.html" target="_blank">Terms and Conditions</a> | <a href="http://whymusicmatters.com/" target="_blank">Why Music Matters</a>
  </div>
</footer>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
<script src="<?php echo esc_url( get_stylesheet_directory_uri()); ?>/dist/bundle.min.js"></script>
</body>
<html>
